#include "interpreter.h"
#include <iostream>

Interpreter::Interpreter()
{

}

void Interpreter::parse(const std::string& function, const std::string& variable)
{
	var_t val  = read_variables[variable]();
	operators[function](val);
}

void Interpreter::assign(const std::string& variable, const std::string& value)
{
	var_t val  = std::stoi(value);
	write_variables[variable](val);
}

void Interpreter::assign(const std::string& variable_1, const std::string& variable_2, const std::string& operator_str, const std::string& variable_3)
{
	var_t val1 = read_variables[variable_2]();
	var_t val2 = read_variables[variable_3]();
	var_t val = std::pair<int,int>{std::get<int>(val1), std::get<int>(val2)};
	var_t result  = operators[operator_str](val);
	write_variables[variable_1](result);
}

var_t Interpreter::a = var_t{};
var_t Interpreter::b = var_t{};
var_t Interpreter::c = var_t{};
var_t Interpreter::d = var_t{};


std::map<std::string, std::function<var_t(var_t)>> Interpreter::operators = {
{"+",[](const auto& operand){ return std::get<std::pair<int,int>>(operand).first + std::get<std::pair<int,int>>(operand).second;}},
{"print",[](const auto& operand){ return print(operand);}}
};

std::map<std::string, std::function<var_t(void)>> Interpreter::read_variables = {
{"a",[](){ return a;}},
{"b",[](){ return b;}},
{"c",[](){ return c;}},
{"d",[](){ return d;}}
};

std::map<std::string, std::function<void(var_t)>> Interpreter::write_variables = {
{"a",[](const auto& operand){ a = operand;}},
{"b",[](const auto& operand){ b = operand;}},
{"c",[](const auto& operand){ c = operand;}},
{"d",[](const auto& operand){ d = operand;}}
};

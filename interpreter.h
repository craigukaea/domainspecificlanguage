#pragma once

#include <functional>
#include <string>
#include <map>
#include <tuple>
#include <cmath>
#include "var_t.h"

class Interpreter
{
public:
    Interpreter();

	void parse(const std::string& function, const std::string& variable);
	void assign(const std::string& variable, const std::string& value);
	void assign(const std::string& variable_1, const std::string& variable_2, const std::string& operator_str, const std::string& variable_3);

private:

	static std::map<std::string, std::function<var_t(var_t)>> operators;
	static std::map<std::string, std::function<var_t(void)>> read_variables;
	static std::map<std::string, std::function<void(var_t)>> write_variables;

    static var_t a, b, c, d;
};

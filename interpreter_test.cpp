#include <iostream>
#include "interpreter.h"

int main()
{
	Interpreter engine;

	engine.assign("a", "2");
	engine.parse("print","a");
	engine.assign("b", "3");
	engine.parse("print","b");
	engine.assign("c","a","+","b");
	engine.parse("print","c");

	return 0;
}

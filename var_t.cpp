#include "var_t.h"
#include <iostream>

int print(const var_t& v)
{
	std::visit(overloaded {
				   [](int i) { std::cout << i << std::endl; },
				   [](std::pair<int,int> i) { std::cout << i.first << "," << i.second << std::endl; }
			   }, v);
	return 0;
}

std::type_index get_contained_type(const var_t v)
{
	return std::visit(overloaded {
						  [](int i) { return std::type_index(typeid(i)); },
						  [](std::pair<int,int> i) { return std::type_index(typeid(i)); }
					  }, v);
}

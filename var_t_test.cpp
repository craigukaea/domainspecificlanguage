#include "var_t.h"
#include <iostream>

int main()
{
	var_t var = std::pair{2,3};
	print(var);
	std::cout << get_contained_type(var).name() << std::endl;
}

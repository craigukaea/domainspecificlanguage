#pragma once
#include <variant>
#include <typeindex>

using var_t = std::variant<int, std::pair<int,int>>;

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

int print(const var_t& v);

std::type_index get_contained_type(const var_t v);
